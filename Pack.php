<?php


class Pack implements Product
{
    /**
     * @var Product []
     */
    private $products = [];

    /**
     * @param Product []
     */
    function __construct($products)
    {
        $this->products = $products;
    }

    /**
     * @return double
     */
    public function getCost()
    {
        $cost = 0;
        foreach ($this->products as $product) {
            $cost += $product->getCost();
        }
        return $cost;
    }
} 