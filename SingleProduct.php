<?php


class SingleProduct implements Product
{
    /**
     * @var double
     */
    protected $cost;

    function __construct($cost)
    {
        $this->cost = $cost;
    }

    /**
     * @return double
     */
    public function getCost()
    {
        return $this->cost;
    }

} 